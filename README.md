#### Generate Openapi documentation while writing RestController with SpringBoot  
##### Demo using Spring Doc with Rest Controller

1   .  add maven dependancy

```
<dependency>
    <groupId>org.springdoc</groupId>
    <artifactId>springdoc-openapi-ui</artifactId>
    <version>1.4.3</version>
</dependency>
```
2   . Create your RestController, mine is [com.example.demo.DemoController](src/main/java/com/example/demo/DemoController.java)

3   . Build & Run the app.

4   . Go to  ```http://localhost:8080/v3/api-docs``` for docs and ```http://localhost:8080/swagger-ui``` for SWAGGER UI

5   . Go further : [https://springdoc.org](https://springdoc.org)


Sure!!!